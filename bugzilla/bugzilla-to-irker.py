#!/usr/bin/python3
# Bugzilla to Irker gateway, SMTP interface
# Portions copied from KDE Git Hooks (git.kde.org:repo-management/hooks/hooklib.py)
import sys, os, socket, re
import configparser
import email.parser
import email.header
try:
    import simplejson as json
except ImportError:
    import json

class BugzillaMessage:
    def __init__(self, receivedEmail):
        # Store this for later
        self._email = receivedEmail

        # Extract the information we need from the Bugzilla email
        # As this comes from bugs.kde.org we can trust it's contents
        # First: is it a new bug or a change to an existing one?
        self.messageType = self.readMailHeader('X-Bugzilla-Type')
        # What product and component is the bug against?
        self.product = self.readMailHeader('X-Bugzilla-Product')
        self.component = self.readMailHeader('X-Bugzilla-Component')
        self.version = self.readMailHeader('X-Bugzilla-Version')
        # Who changed it, and who is assigned to it?
        self.changedBy = self.readMailHeader('X-Bugzilla-Who')
        self.assignedTo = self.readMailHeader('X-Bugzilla-Assigned-To')
        # What is the status of this bug?
        self.status = self.readMailHeader('X-Bugzilla-Status')
        # What is the priority, severity and keywords associated with the bug?
        self.priority = self.readMailHeader('X-Bugzilla-Priority')
        self.severity = self.readMailHeader('X-Bugzilla-Severity')
        self.keywords = self.readMailHeader('X-Bugzilla-Keywords')

        # Extract the short name and number of the bug from the email subject
        # This comes in the form of: 
        # [product] [Bug number] Short name
        emailSubject = self.readMailHeader('Subject')
        subjectContents = re.match("\[([^\[]+)\] \[Bug ([^\[]+)\] (New: )?(.+)", emailSubject)
        self.bugNumber = subjectContents.group(2)
        self.shortDescription = subjectContents.group(4)

    def readMailHeader(self, headerName):
        # Sometimes Email headers are encoded in UTF-8
        # Unfortunately the email module from Python doesn't decode these for us, so we need to do it
        # First, decode the header we've been given
        decodedHeader = email.header.decode_header( self._email[ headerName ] )

        # Now we glue the various parts we received above
        # This is required because different parts of an email header can be encoded differently and are thus returned separately by decode_header
        # We need a whole string to work with though
        sanitizedHeaderParts = []
        for partContent, partEncoding in decodedHeader:
            # First, if we have straight text there is nothing for us to do
            # Just add it to the list of sanitized headers
            if type(partContent) is str:
                sanitizedHeaderParts.append( partContent )
                continue

            # Otherwise we need to decode it first!
            sanitizedHeaderParts.append( partContent.decode(partEncoding) )

        # Finally, combine the sanitized header parts
        readableHeader = "".join( sanitizedHeaderParts )
        return readableHeader

    def summary(self, compact):
        # For new bugs we use a different template (just for convenience)
        if compact:
            if self.messageType == 'new':
                template = 'New bug %(bugNumber)s in %(product)s (%(component)s) filed by %(changedBy)s [%(priority)s - %(severity)s]: %(shortDescription)s (https://bugs.kde.org/%(bugNumber)s)'
            else:
                template = 'Bug %(bugNumber)s in %(product)s (%(component)s) changed by %(changedBy)s [%(priority)s - %(severity)s - %(status)s]: %(shortDescription)s (https://bugs.kde.org/%(bugNumber)s)'
        else:
            if self.messageType == 'new':
                template = 'New bug %(bugNumber)s in %(product)s (%(component)s) filed by %(changedBy)s [%(priority)s - %(severity)s]:\n%(shortDescription)s\nhttps://bugs.kde.org/%(bugNumber)s'
            else:
                template = 'Bug %(bugNumber)s in %(product)s (%(component)s) changed by %(changedBy)s [%(priority)s - %(severity)s - %(status)s]:\n%(shortDescription)s\nhttps://bugs.kde.org/%(bugNumber)s'

        return template % self.__dict__

# Load our configuration
configDefaults = {
    'product': '.*',
    'assignee': '.*',
    'priority': '.*',
    'severity': '.*',
    'messageType': '.*',
    'compact': 'False'
}
config = configparser.SafeConfigParser(configDefaults)
config.read(sys.argv[1])

# Read the email in
emailParser = email.parser.Parser()
parsedEmail = emailParser.parse(sys.stdin)

# Ensure it is a Bugzilla formatted mail?
if not ('X-Bugzilla-URL' in parsedEmail and parsedEmail['X-Bugzilla-URL'] == "http://bugs.kde.org/"):
    exit(0)

# Parse the message we received from KDE Bugzilla (bugs.kde.org)
message = BugzillaMessage(parsedEmail)

# Establish a connection to Irker
connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connection.connect(("localhost", 6659))

# Determine the channels we want to notify
channels = []
for sectionName in config.sections():
    # Try to compile the regexes - if it fails, continue on...
    try:
        productName = re.compile( config.get(sectionName, 'product') )
        assigneeEmail = re.compile( config.get(sectionName, 'assignee') )
        priorityLevel = re.compile( config.get(sectionName, 'priority') )
        severityLevel = re.compile( config.get(sectionName, 'severity') )
        messageType = re.compile( config.get(sectionName, 'messageType') )
        compact = config.getboolean(sectionName, 'compact')
    except:
        continue

    if not config.has_option(sectionName, 'channel'):
        continue

    if not productName.match(message.product):
        continue

    if not assigneeEmail.match(message.assignedTo):
        continue

    if not priorityLevel.match(message.priority):
        continue

    if not severityLevel.match(message.severity):
        continue

    if not messageType.match( message.messageType ):
        continue

    # It's time to send the message, but make first let's make sure we don't spam the same channel twice...
    channelName = config.get(sectionName, 'channel')
    if channelName not in channels:
        # Mark this channel as having received a notification
        channels.append(channelName)

        # Notify the channel
        content = json.dumps({"to": [channelName], "privmsg": message.summary(compact)}) + "\n"
        connection.sendall(content.encode('utf-8'))
