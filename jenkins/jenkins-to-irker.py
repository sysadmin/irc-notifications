#!/usr/bin/python3
# Jenkins to Irker gateway, SMTP interface
# Portions copied from KDE Git Hooks (git.kde.org:repo-management/hooks/hooklib.py)
import sys, os, socket, re
import configparser
import email.parser
import email.policy
try:
    import simplejson as json
except ImportError:
    import json

class JenkinsNotification:
    def __init__(self, receivedEmail):
        # Store this for later
        self._email = receivedEmail

        # Extract the information we need from the Jenkins email
        # As this comes from build.kde.org we can trust it's contents
        # Everything we do here comes from the message subject....
        # Begin the extraction process. We want to grab the Product, Project, Platform, Job Number and the Build Status
        # Example: KDE CI: Extragear ring-kde kf5-qt5 SUSEQt5.9 - Build # 2 - Still Failing!
        emailSubject = email['Subject'].replace('\n', '')
        subjectContents = re.match("^KDE CI: ([^ ]+) » ([^ ]+) » ([^ ]+) ([^ ]+) - Build # ([0-9]+) - (.*)$", emailSubject)
        # Grab all the bits of information we've gathered...
        self.product = subjectContents.group(1)
        self.project = subjectContents.group(2)
        self.branch  = subjectContents.group(3)
        self.platform = subjectContents.group(4)
        self.buildNumber = subjectContents.group(5)
        self.buildStatus = subjectContents.group(6)

    def summary(self):
        # Construct the message we need to print to IRC
        messageTemplate = '\x02Build #%(buildNumber)s for %(project)s in %(product)s on %(platform)s\x02 \x035(branch %(branch)s)\x03 - \x02%(buildStatus)s\x02\n'
        urlTemplate = 'https://build.kde.org/job/%(product)s/job/%(project)s/job/%(branch)s %(platform)s/%(buildNumber)s/'
        # Trigger variable substitution for both parts...
        messageText = messageTemplate % self.__dict__
        urlText = urlTemplate % self.__dict__
        # Replace any spaces in the URL...
        urlText = urlText.replace(' ', '%20')
        # Fuse the two parts together
        return messageText + urlText

# Load our configuration
configDefaults = {'product': '.*', 'project': '.*', 'platform': '.*', 'status': '.*'}
config = configparser.SafeConfigParser(configDefaults)
config.read(sys.argv[1])

# Read the email in
emailParser = email.parser.Parser(policy=email.policy.SMTP)
email = emailParser.parse(sys.stdin)

# Ensure it is a Jenkins formatted mail
if not 'X-Jenkins-Job' in email:
    exit(0)

# Parse the message we received from KDE Continuous Integration (build.kde.org)
message = JenkinsNotification(email)

# Determine the channels we want to notify
channels = []
for sectionName in config.sections():
    # Try to compile the regexes - if it fails, continue on...
    try:
        productCheck = re.compile( config.get(sectionName, 'product') )
        projectCheck = re.compile( config.get(sectionName, 'project') )
        platformCheck = re.compile( config.get(sectionName, 'platform') )
        buildStatusCheck = re.compile( config.get(sectionName, 'status') )
    except:
        continue

    if not config.has_option(sectionName, 'channel'):
        continue

    if not productCheck.match(message.product):
        continue

    if not projectCheck.match(message.project):
        continue

    if not platformCheck.match(message.platform):
        continue

    if not buildStatusCheck.match(message.buildStatus):
        continue

    channelName = config.get(sectionName, 'channel')
    if channelName not in channels:
        channels.append(channelName)

# Convert our information into what Irker needs, and send it to Irker
content = json.dumps({"to": channels, "privmsg": message.summary()}) + "\n"
connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connection.connect(("localhost", 6659))
connection.sendall(content.encode('utf-8'))
